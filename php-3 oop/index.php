<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$shaun = new Animal("shaun");

echo "Nama Hewan : " . $shaun->hewan . "<br>";
echo "Jumlah Kaki : " . $shaun->leg . "<br>";
echo "Cold Blooded : " . $shaun->cold_blooded . "<br>";

echo "<br>";

$kodok = new Kodok('buduk');
echo "Nama Hewan : " . $kodok->hewan . "<br>";
echo "Jumlah Kaki : " . $kodok->leg . "<br>";
echo "Cold Blooded : " . $kodok->cold_blooded . "<br>";
echo "Jump : " . $kodok->jump . "<br>";

echo "<br>";

$sungokong = new Ape('kera sakti');
echo "Nama Hewan : " . $sungokong->hewan . "<br>";
echo "Jumlah Kaki : " . $sungokong->leg . "<br>";
echo "Cold Blooded : " . $sungokong->cold_blooded . "<br>";
echo "Yell : " . $sungokong->yell . "<br>";

echo "<br>";

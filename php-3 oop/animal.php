<?php

class Animal
{
    public $leg = 4;
    public $cold_blooded = "no";
    public $jump = "Hop Hop";
    public $yell = "Auooo";
    public $hewan;

    public function __construct($nama)
    {
        $this->hewan = $nama;
    }
}

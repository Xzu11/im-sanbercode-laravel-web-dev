@extends('layouts.master')
@section('title')
    Halaman Tambah Data Cast
@endsection
@section('content')

<h1>Create Post</h1>
 
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="/cast" method="post">
    @csrf
    <div class="mb-3">
    <label>Nama</label>
    <input type="text" name="nama" class="form-control">
    <div class="form-text"></div>
    </div>
    <br>    
    <div class="mb-3">
    <label>Umur</label>
    <input type="text" name="umur" class="form-control">
    <div class="form-text"></div>
    </div>
    <br>    
    <div class="mb-3">
    <label>Bio</label>
    <textarea name="bio" class="form-control" cols="30" rows="10"></textarea>
    <div class="form-text"></div>
    </div>
    <br>
    <button class="btn btn-success">Simpan</button>
</form>

@endsection
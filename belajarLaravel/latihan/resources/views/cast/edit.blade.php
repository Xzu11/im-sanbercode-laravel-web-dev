@extends('layouts.master')
@section('title')
    Halaman Edit Data Cast
@endsection
@section('content')

<h1>Edit Post</h1>
 
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="/cast/{{ $peran->id }}" method="post">
    @csrf
    @method('put');
    <div class="mb-3">
    <label>Nama</label>
    <input type="text" name="nama" value="{{ $peran->nama }}" class="form-control">
    <div class="form-text"></div>
    </div>
    <br>    
    <div class="mb-3">
    <label>Umur</label>
    <input type="text" name="umur" value="{{ $peran->umur }}" class="form-control">
    <div class="form-text"></div>
    </div>
    <br>    
    <div class="mb-3">
    <label>Bio</label>
    <textarea name="bio" class="form-control" cols="30" rows="10">{{ $peran->bio }}</textarea>
    <div class="form-text"></div>
    </div>
    <br>
    <button class="btn btn-success">Simpan</button>
</form>

@endsection
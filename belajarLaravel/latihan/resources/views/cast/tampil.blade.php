@extends('layouts.master')
@section('title')
    Halaman Tampil Data Cast
@endsection
@section('content')

<a href="/cast/create"><button class="btn btn-primary my-3">Tambah</button></a>

<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Aksi</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $people)
        <tr>
            <th scope="row">{{ $key+1 }}</th>
            <td>{{ $people->nama }}</td>
            <td>{{ $people->umur }}</td>
            <td>
              <form action="/cast/{{ $people->id }}" method="post">
                <a href="/cast/{{ $people->id }}" class="btn btn-info">Detail</a>
                <a href="/cast/{{ $people->id }}/edit" class="btn btn-warning">Edit</a>
                @csrf
                @method('delete')
                <input type="submit" value="Delete" class="btn btn-danger">
                </form>
            </td>
          </tr>
        @empty
            <tr>
                <td>Dih buset kagak ada datanya!</td>
            </tr>
        @endforelse
    </tbody>
  </table>

@endsection
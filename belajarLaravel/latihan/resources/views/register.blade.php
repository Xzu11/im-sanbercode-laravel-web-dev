@extends('layouts.master')
@section('title')
    Halaman Daftar
@endsection
@section('content')
    <h1>Buat Account Baru</h1>
    <h2>Sign Up Form</h2>
    <form action="/daftar" method="post">
        @csrf
        First Name:
        <input type="text" name="fname">
        <br><br>
        Lasr Name:
        <input type="text" name="lname">
        <br><br>
        Gender:
        <br>
        <input type="radio" name="gender">Male <br>
        <input type="radio" name="gender">Female <br>
        <input type="radio" name="gender">Other <br>

        <br>

        Nationaly : <select name="nationaly">
            <option value="indonesia">Indonesia</option>
            <option value="japanese">Japanese</option>
            <option value="korea">Korea</option>
        </select>
        <br><br>

        Language Spoken: 
        <br>
        <input type="checkbox" name="language">Bahasa Indonesia <br>
        <input type="checkbox" name="language">English <br>
        <input type="checkbox" name="language">Japanese <br>
        <input type="checkbox" name="language">Korean <br>

        <br>

        Bio: 
        <br>
        <textarea name="bio" cols="30" rows="10"></textarea>

        <br><br>
        <button>Sign Up</button>
    </form>
    @endsection
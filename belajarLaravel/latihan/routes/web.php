<?php

use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CastController;
use PhpParser\Node\Expr\Cast;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home']);

Route::get('/register', [AuthController::class, 'register']);

Route::post('/daftar', [AuthController::class, 'daftar']);

Route::get('/data-table', function () {
    return view('datatable');
});

Route::get('/table', function () {
    return view('table');
});

// CRUD Routes

// C => Create
// Route untuk mengarah ke form inputan tambah cast
Route::get('/cast/create', [CastController::class, 'create']);
// route simpan.
Route::post('/cast', [CastController::class, 'store']);

// R => Read
// just like what i said before, the read means to select all the data in ur table/database. except u want read some data what u want.
Route::get('/cast', [CastController::class, 'index']);
// Route data for the detail Bio according in parameter id.
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

// U => Update
// route yang mengarah ke form update cast berdasarkan id.
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);
// untuk update data ke DB table cast berdasarkan id.
Route::put('/cast/{id}', [CastController::class, 'update']);

// D => Delete
// gak usah dijelasin keknya ngerti gunanya DELETE buat apa.
Route::delete('/cast/{id}', [CastController::class, 'destroy']);
